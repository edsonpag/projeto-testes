const main = require('./main');

describe('Equilateros', () => {

    test('Todos iguais', () => {
        expect(main.equilatero(1, 1, 1)).toBe(true);
    });

    test('Todos diferentes', () => {
        expect(main.equilatero(1, 2, 3)).toBe(false);
    });

    test('Dois iguais e um diferente', () => {
        expect(main.equilatero(1, 2, 1)).toBe(false);
    });

    test('Valores invalidos', () => {
        expect(main.equilatero(1, 'a', 3)).toBe(false);
    });

    test('Valores invalidos', () => {
        expect(main.equilatero(undefined, NaN, 3)).toBe(false);
    });

});

describe('Escalenos', () => {

    test('Valores diferenets', () => {
        expect(main.escaleno(1, 2, 3)).toBe(true);
    });

    test('Valores iguais', () => {
        expect(main.escaleno(1, 1, 1)).toBe(false);
    });

    test('Dois valores iguais e um diferente', () => {
        expect(main.escaleno(1, 1, 3)).toBe(false);
    });

    test('Valor invalido', () => {
        expect(main.escaleno(1, 1, 'a')).toBe(false);
    });

});

describe('Isosceles', () => {

    test('Dois valores iguais e um diferente', () => {
        expect(main.isosceles(1, 1, 3)).toBe(true);
    });

    test('Todos os valores iguais', () => {
        expect(main.isosceles(1, 1, 1)).toBe(false);
    });

    test('Todos os valores diferentes', () => {
        expect(main.isosceles(1, 2, 4)).toBe(false);
    });

    test('Valores invalidos', () => {
        expect(main.isosceles(1, 'a', '4')).toBe(false);
    });
});


