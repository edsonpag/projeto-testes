const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function equilatero(num1, num2, num3) {

    return (num1 === num2 && num1 === num3) ? true : false
}

function escaleno(num1, num2, num3) {

    return (num1 !== num2 && num1 !== num3 && num2 !== num3) ? true : false
}

function isosceles(num1, num2, num3) {

    if(num1 === num2 && num1 !== num3) {
        return true;
    } else if(num2 === num3 && num2 !== num1) {
        return true;
    } else if(num1 === num3 && num1 !== num2) {
        return true;
    } else {
        return false;
    }

}

function main() {

    var type = '';

    rl.question('Digite o valor do primeiro lado do triângulo\n', (answer1) => {

        rl.question('Digite o valor do segundo lado do triângulo\n', (answer2) => {
    
            rl.question('Digite o valor do terceiro lado do triângulo\n', (answer3) => {

                if(isNaN(answer1) || isNaN(answer2) || isNaN(answer3)) {
                    console.log("Digite apenas numeros");
                    type = 'Invalido'
                } else {

                    if(equilatero(answer1, answer2, answer3)) {
                        type = 'Equilatero';
                    } else if(escaleno(answer1, answer2, answer3)) {
                        type = 'Escaleno';
                    } else if(isosceles(answer1, answer2, answer3)) {
                        type = 'Isosceles'
                    } else {
                        type = 'Invalido';
                    }
                }

                console.log(`O tipo do triângulo é: ${type}`);
                rl.close();
            });   
        });  
    });
}

console.log("---- Programa que lê o valor dos três lados do triângulo e retorna seu tipo ----");
main();

module.exports = {
    equilatero: equilatero,
    escaleno: escaleno,
    isosceles: isosceles
}